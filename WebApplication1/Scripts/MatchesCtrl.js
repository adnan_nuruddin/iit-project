﻿(function () {
	'use strict';
	angular.module('LEAGUE')
	.controller('MatchesCtrl', ['$scope','$http','matchService', function ($scope,$http,matchService) {
		$scope.pageMessage = 'THIS MESSAGE CAME FROM A CONTROLLER';
		$scope.matchSubmitData = {};
		$scope.getTeams = function () {
			$http.get('/Teams/getAllTeams')
			.then(function(response) {
				$scope.teams = response.data;
			}).catch(function(response) {
				console.log(response);
			});
		};

		$scope.getTeams();

		$scope.getMatches = function () {
			$http.get('/Matches/getAllMatches')
			.then(function(response) {
				$scope.matches = response.data;
			}).catch(function(response) {
				console.log(response);
			});
		};

		$scope.selectedVideo = "";
		$scope.setVideo = function(highlightsUrl) {
			$scope.selectedVideo = highlightsUrl;
		}

		$scope.getMatches();

		$scope.deleteMatch = function (match_id) {
			$http.get('/Matches/deleteMatch?match_id='+match_id)
			.then(function(response) {
				$scope.getMatches();
				alert("Match Deleted.");
			}).catch(function(response) {
				alert("Something Went Wrong. Please try again.");
				console.log(response);
			});
		};

		$scope.saveMatch = function (matchSubmitData) {
			matchService.saveMatch(matchSubmitData)
			.then(function (data) {
				$scope.matchSubmitData = {};
				$scope.getMatches();
				alert("Match Added Successfuly");
			}).catch(function() {
				alert("Something Went Wrong. Please Try again.");
			});
		};

		$scope.submitMatchData = function() {
			$http({
				method: 'POST',
				url: '/Home/saveMatchData/',
				data: $.param($scope.matchSubmitData),
				headers: {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			}).then(function successCallback(response) {
				alert("Success");
				console.log(response);
				$scope.getMatches();
			}, function errorCallback(response) {
				console.log(response);
				alert("Failed");
			});
		}

	}])
})();