﻿(function () {
	'use strict';
	angular.module("akFileUploader", []).factory("akFileUploaderService", ["$q", "$http",
		function ($q, $http) {

			var getModelAsFormData = function (data) {
				var dataAsFormData = new FormData();
				angular.forEach(data, function (value, key) {
					dataAsFormData.append(key, value);
				});
				return dataAsFormData;
			};

			var saveModel = function (data, url) {
				var deferred = $q.defer();
				$http({
					url: url,
					method: "POST",
					data: getModelAsFormData(data),
					transformRequest: angular.identity,
					headers: { 'Content-Type': undefined }
				}).success(function (result) {
					deferred.resolve(result);
				}).error(function (result, status) {
					deferred.reject(status);
				});
				return deferred.promise;
			};

			return {
				saveModel: saveModel
			}}])
	.directive("akFileModel", ["$parse",
		function ($parse) {
			return {
				restrict: "A",
				link: function (scope, element, attrs) {
					var model = $parse(attrs.akFileModel);
					var modelSetter = model.assign;
					element.bind("change", function () {
						scope.$apply(function () {
							modelSetter(scope, element[0].files[0]);
						});
					});
				}
			};
		}]);

				angular.module('LEAGUE', ['ngRoute', 'oc.lazyLoad','akFileUploader']).config(['$routeProvider', function ($routeProvider) {
					$routeProvider
					.when('/', {
						controller: 'HomeCtrl',
						templateUrl: "/Teams",
						resolve: {
							deps: ['$ocLazyLoad', function ($ocLazyLoad) {
								return $ocLazyLoad.load([{
									files: [
									'/scripts/HomeCtrl.js'
									]
								}]);
							}]
						}
					}).when('/Matches', {
						controller: 'MatchesCtrl',
						templateUrl: "/Matches",
						resolve: {
							deps: ['$ocLazyLoad', function ($ocLazyLoad) {
								return $ocLazyLoad.load([{
									files: [
									'/scripts/MatchesCtrl.js'
									]
								}]);
							}]
						}
					}).when('/Standings', {
						controller: 'StandingsCtrl',
						templateUrl: "/Home/Standings",
						resolve: {
							deps: ['$ocLazyLoad', function ($ocLazyLoad) {
								return $ocLazyLoad.load([{
									files: [
									'/scripts/StandingsCtrl.js'
									]
								}]);
							}]
						}
					}).otherwise({ redirectTo: '/' });
				}])

				.config(['$httpProvider', function ($httpProvider) {
					$httpProvider.interceptors.push('authInterceptor');
				}]).factory('authInterceptor', ['$rootScope', '$q', function ($rootScope, $q) {
					var interceptor = {
						request: function (config) {
                // if (config.method === 'POST' && config.headers['Content-Type']!='multipart/form-data') {
                //     config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
                //     config.data = $.param(config.data);
                // }
                return config;
            },
            response: function (response) {
            	return response;
            },
            responseError: function (response, error) {

            }
        };
        return interceptor;
    }]).factory("teamService", ["akFileUploaderService", function (akFileUploaderService) {
    	var saveTeam = function (team) {
    		return akFileUploaderService.saveModel(team, "/Teams/Save");
    	};
    	return {
    		saveTeam: saveTeam
    	};
    }])
    .factory("matchService", ["akFileUploaderService", function (akFileUploaderService) {
    	var saveMatch = function (match) {
    		return akFileUploaderService.saveModel(match, "/Matches/Save");
    	};
    	return {
    		saveMatch: saveMatch
    	};
    }])

})();