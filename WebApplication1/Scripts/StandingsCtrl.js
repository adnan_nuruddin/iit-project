﻿(function () {
	'use strict';
	angular.module('LEAGUE')
	.controller('StandingsCtrl', ['$scope','$http', function ($scope,$http) {
		$scope.pageMessage = 'THIS MESSAGE CAME FROM A CONTROLLER';

		$scope.getStandings = function () {
			$http.get('/Home/getStandings')
			.then(function(response) {
				console.log(response);
				$scope.standings = response.data;
			}).catch(function(response) {
				console.log(response);
			});
		};

		$scope.deleteTeam = function (team_id) {
			$http.get('/Teams/Delete?team_id='+team_id)
			.then(function(response) {
				$scope.getStandings();
				alert("Team Deleted.");
			}).catch(function(response) {
				alert("Something Went Wrong. Please try again.");
				console.log(response);
			});
		};

		$scope.getStandings();
	}])
})();