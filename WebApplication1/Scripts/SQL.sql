﻿--phpMyAdmin SQL Dump
--version 4.5.1
--http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
--Generation Time: Dec 19, 2017 at 11:32 AM
--Server version: 10.1.19- MariaDB
--PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `league`
--

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

    CREATE TABLE `matches`(
        `match_id` int(11) NOT NULL,
        `home_team` int(11) NOT NULL,
        `away_team` int(11) NOT NULL,
        `home_goal` int(11) NOT NULL,
        `away_goal` int(11) NOT NULL,
        `game_week` int(11) NOT NULL,
        `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `highlights` varchar(45)
    ) ENGINE= InnoDB DEFAULT CHARSET= latin1;

--
-- Dumping data for table `matches`
--

    INSERT INTO `matches`(`match_id`, `home_team`, `away_team`, `home_goal`, `away_goal`, `game_week`, `date`) VALUES
        (1, 1, 2, 3, 0, 1, '2017-12-19 12:48:40'),
        (2, 2, 1, 3, 4, 21, '2017-12-19 12:48:40'),
        (3, 1, 3, 5, 3, 0, '2017-12-19 15:36:27'),
        (4, 3, 4, 1, 2, 0, '2017-12-19 15:40:21'),
        (5, 2, 3, 4, 1, 0, '2017-12-19 15:45:07'),
        (6, 3, 5, 1, 0, 0, '2017-12-19 15:46:16'),
        (7, 3, 1, 3, 1, 0, '2017-12-19 16:01:12');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

    CREATE TABLE `teams`(
        `team_id` int(11) NOT NULL,
        `team_name` varchar(245) NOT NULL,
        `team_logo` varchar(245) DEFAULT NULL,
        `remarks` text
    ) ENGINE= InnoDB DEFAULT CHARSET= latin1;

--
-- Dumping data for table `teams`
--

    INSERT INTO `teams`(`team_id`, `team_name`, `team_logo`, `remarks`) VALUES
        (1, 'Arsenal F.C.', NULL, 'Arsenal Football Club is a professional football club based in Highbury, London, England, that plays in the Premier League, the top flight of English football.'),
        (2, 'Chelsea', NULL, 'Chelsea Football Club is a professional football club based in London, England that competes in the Premier League, of which they are reigning champions. Founded in 1905, the clubs home ground since then has been Stamford Bridge in Fulham.'),
        (3, 'Manchester United F.C.', NULL, 'Manchester United Football Club is a professional football club based in Old Trafford, Greater Manchester, England, that competes in the Premier League, the top flight of English football.'),
        (4, 'Manchester City', NULL, 'Manchester City Football Club are a football club in Manchester, England. Founded in 1880 as St. Marks, they became Ardwick Association Football Club in 1887 and Manchester City in 1894.'),
        (5, 'Tottenham Hotspur F.C.', NULL, 'Tottenham Hotspur Football Club, commonly referred to simply as Tottenham or Spurs, is an English football club located in Tottenham, Haringey, London, that competes in the Premier League.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `matches`
--
    ALTER TABLE `matches`
  ADD PRIMARY KEY (`match_id`);

--
-- Indexes for table `teams`
--
    ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `matches`
--
    ALTER TABLE `matches`
  MODIFY `match_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 8;
--
-- AUTO_INCREMENT for table `teams`
--
    ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
