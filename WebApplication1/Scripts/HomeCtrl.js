﻿(function () {
	'use strict';
	angular.module('LEAGUE')
	.controller('HomeCtrl', ['$scope','$http',"teamService", function ($scope,$http,teamService) {
		$scope.teamSubmitData = {};

		$scope.saveTeam = function (teamSubmitData) {
			teamService.saveTeam(teamSubmitData)
			.then(function (data) {
				alert("Team Added Successfuly");
				$scope.teamSubmitData = {};
			}).catch(function() {
				alert("Something Went Wrong. Please Try again.");
			});
		};
	}])
})();