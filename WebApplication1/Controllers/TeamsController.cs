﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TeamsController : Controller
    {
        // GET: Teams
        public ActionResult Index()
        {
            return PartialView();
        }

        public ActionResult Save(TeamModel teamData)
        {
            var fileName = Guid.NewGuid().ToString("n") + ".png";
            foreach (string file in Request.Files)
            {
                var fileContent = Request.Files[file];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    var inputStream = fileContent.InputStream;
                    var path = Path.Combine(Server.MapPath("~/Content/images"), fileName);
                    using (var fileStream = System.IO.File.Create(path))
                    {
                        inputStream.CopyTo(fileStream);
                    }
                }
            }

            teamData.team_name = Request.Form["team_name"];
            teamData.team_logo = fileName;
            teamData.remarks = Request.Form["remarks"];
            if (teamData.Insert())
            {
                return Json(teamData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Something went wrong";
                return Json("Server Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete(int team_id)
        {
            TeamModel teamData = new TeamModel();
            teamData.team_id = team_id;
            if (teamData.Delete())
            {
                return Json(teamData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Something went wrong";
                return Json("Server Error", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult getAllTeams()
        {
            return Json(TeamModel.getAll(), JsonRequestBehavior.AllowGet);
        }
    }
}