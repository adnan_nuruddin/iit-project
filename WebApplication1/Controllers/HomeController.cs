﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Data.Odbc;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.Helpers;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Standings()
        {
            return PartialView("Standings");
        }

        public void getStandings()
        {
            string sqlCommand = @"select teams.team_id, team_name, team_logo,
            IFNULL(sum(matches.home_goal),0) as home_goal_scored,
            IFNULL(sum(matches.away_goal),0) as home_goal_conceded, 
            count(IF(matches.home_goal>matches.away_goal,1,NULL)) as home_win, 
            count(IF(matches.home_goal=matches.away_goal,1,NULL)) as home_draw, 
            count(IF(matches.home_goal<matches.away_goal,1,NULL)) as home_lose,
            away.away_goal_scored,
            away.away_goal_conceded,  
            away.away_win, 
            away.away_draw, 
            away.away_lose
            from teams
            left join matches on teams.team_id = matches.home_team
            left join 
            (
                  SELECT
                  IFNULL(sum(matches.away_goal),0) as away_goal_scored,
                  IFNULL(sum(matches.home_goal),0) as away_goal_conceded,  
                  count(IF(matches.home_goal<matches.away_goal,1,NULL)) as away_win, 
                  count(IF(matches.home_goal=matches.away_goal,1,NULL)) as away_draw, 
                  count(IF(matches.home_goal>matches.away_goal,1,NULL)) as away_lose,
                  teams.team_id
                  from teams
                  left join matches on teams.team_id = matches.away_team
                  group by teams.team_id
            )
            away on teams.team_id = away.team_id
            group by teams.team_id";

            Response.AppendHeader("Content-Type", "application/json");

            try
            {
                using (OdbcConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString))
                {
                    connection.Open();
                    using (OdbcCommand command = new OdbcCommand(sqlCommand, connection))
                    using (OdbcDataReader dr = command.ExecuteReader())
                    {
                        Response.Write("[");
                        int count = 0;
                        while (dr.Read())
                        {
                            if (count != 0) { Response.Write(","); }

                            Response.Write("{");
                            Response.Write("\"team_id\" :"); Response.Write("\"" + dr["team_id"].ToString() + "\",");
                            Response.Write("\"team_name\" :"); Response.Write("\"" + dr["team_name"].ToString() + "\",");
                            Response.Write("\"team_logo\" :"); Response.Write("\"" + Url.Content(@"~/Content/images/"+ dr["team_logo"].ToString()).ToString()+ "\",");
                            Response.Write("\"home_goal_scored\" :"); Response.Write("\"" + dr["home_goal_scored"].ToString() + "\",");
                            Response.Write("\"home_goal_conceded\" :"); Response.Write("\"" + dr["home_goal_conceded"].ToString() + "\",");
                            Response.Write("\"home_win\" :"); Response.Write("\"" + dr["home_win"].ToString() + "\",");
                            Response.Write("\"home_draw\" :"); Response.Write("\"" + dr["home_draw"].ToString() + "\",");
                            Response.Write("\"home_lose\" :"); Response.Write("\"" + dr["home_lose"].ToString() + "\",");
                            Response.Write("\"away_goal_scored\" :"); Response.Write("\"" + dr["away_goal_scored"].ToString() + "\",");
                            Response.Write("\"away_goal_conceded\" :"); Response.Write("\"" + dr["away_goal_conceded"].ToString() + "\",");
                            Response.Write("\"away_win\" :"); Response.Write("\"" + dr["away_win"].ToString() + "\",");
                            Response.Write("\"away_draw\" :"); Response.Write("\"" + dr["away_draw"].ToString() + "\",");
                            Response.Write("\"away_lose\" :"); Response.Write("\"" + dr["away_lose"].ToString() + "\"");
                            Response.Write("}");

                            count++;
                        }

                        Response.Write("]");
                        dr.Close();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Response.Write("an error occured: " + ex.Message);
            }
        }

    }
}