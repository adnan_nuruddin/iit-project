﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class MatchesController : Controller
    {
        // GET: Matches
        public ActionResult Index()
        {
            return PartialView("Index");
        }

        public ActionResult Save(MatchModel MatchData)
        {
            var fileName = Guid.NewGuid().ToString("n") + ".mp4";
            foreach (string file in Request.Files)
            {
                var fileContent = Request.Files[file];
                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    var inputStream = fileContent.InputStream;
                    var path = Path.Combine(Server.MapPath("~/Content/video"), fileName);
                    using (var fileStream = System.IO.File.Create(path))
                    {
                        inputStream.CopyTo(fileStream);
                    }
                }
            }

            MatchData.home_team = Int32.Parse(Request.Form["home_team"]);
            MatchData.away_team = Int32.Parse(Request.Form["away_team"]);
            MatchData.highlights = fileName;
            MatchData.home_goal = Int32.Parse(Request.Form["home_goal"]);
            MatchData.away_goal = Int32.Parse(Request.Form["away_goal"]);
            
            if (MatchData.Insert())
            {
                return Json(MatchData);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Something went wrong";
                return Json("Server Error");
            }
        }

        public ActionResult getAllMatches()
        {
            String imageLocation = Url.Content(@"~/Content/images/");
            String videoLocation = Url.Content(@"~/Content/video/");
            return Json(MatchModel.getAll(imageLocation,videoLocation),JsonRequestBehavior.AllowGet);
        }

        public ActionResult deleteMatch(int match_id)
        {
            MatchModel matchData = new MatchModel();
            matchData.match_id = match_id;
            if (matchData.Delete())
            {
                return Json(matchData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "Something went wrong";
                return Json("Server Error",JsonRequestBehavior.AllowGet);
            }
        }
    }
}