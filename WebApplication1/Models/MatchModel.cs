﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApplication1.Models
{
	public class MatchModel
	{
		public int match_id;
		public int home_team;
		public int away_team;
		public string highlights;
		public string date;
		public string home_team_logo;
		public string away_team_logo;
		public string home_team_name;
		public string away_team_name;
		public int home_goal;
		public int away_goal;

		public bool Insert()
		{
			string sqlCommand = @"INSERT INTO `matches` (`home_team`, `away_team`,`home_goal`,`away_goal`,`highlights`) VALUES ('" + this.home_team + "', '" + this.away_team + "', '" + this.home_goal + "', '" + this.away_goal + "','"+this.highlights+"');";

			try
			{
				using (OdbcConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString))
				{
					connection.Open();
					using (OdbcCommand command = new OdbcCommand(sqlCommand, connection))
					{
						command.ExecuteReader();
					}
					connection.Close();
					return true;
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				return false;
			}
		}

		public bool Delete()
		{
			string sqlCommand = "DELETE FROM matches where match_id = " + this.match_id;

			try
			{
				using (OdbcConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString))
				{
					connection.Open();

					OdbcCommand command = new OdbcCommand(sqlCommand, connection);
					command.ExecuteNonQuery();
					connection.Close();
					return true;
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				return false;
			}
		}

		public static ArrayList getAll(string imageLocation,string videoLocation) {
			ArrayList allMatches = new ArrayList();

			string sqlCommand = @"select match_id, DATE_FORMAT(`date`,'%a, %M %d, %Y') as `date`, highlights,
			home.team_logo as home_team_logo, away.team_logo as away_team_logo, 
			home.team_id as home_team, home.team_name as home_team_name, 
			away.team_id as away_team,away.team_name as away_team_name, 
			home_goal, away_goal, game_week from matches 
			LEFT JOIN teams as `home` ON home.team_id = matches.home_team
			LEFT JOIN teams as `away` ON away.team_id = matches.away_team;";

			try
			{
				using (OdbcConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString))
				{
					connection.Open();
					using (OdbcCommand command = new OdbcCommand(sqlCommand, connection))
					using (OdbcDataReader dr = command.ExecuteReader())
					{
						while (dr.Read())
						{
							MatchModel m = new MatchModel();
							m.match_id = Int32.Parse(dr["match_id"].ToString());
							m.home_team = Int32.Parse(dr["home_team"].ToString());
							m.away_team = Int32.Parse(dr["away_team"].ToString());
							m.date = dr["date"].ToString();
							m.home_team_logo = imageLocation + dr["home_team_logo"].ToString();
							m.away_team_logo = imageLocation + dr["away_team_logo"].ToString();
							m.home_team_name = dr["home_team_name"].ToString();
							m.away_team_name = dr["away_team_name"].ToString();
							m.home_goal = Int32.Parse(dr["home_goal"].ToString());
							m.away_goal = Int32.Parse(dr["away_goal"].ToString());
							m.highlights = videoLocation + dr["highlights"].ToString();

							allMatches.Add(m);
						}

						dr.Close();
					}
					connection.Close();
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
				return allMatches;
			}


			return allMatches;
		}

	}
}