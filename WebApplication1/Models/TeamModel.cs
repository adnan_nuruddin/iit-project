﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Odbc;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class TeamModel
    {
        public int team_id;
        public string team_name;
        public string team_logo;
        public string remarks;

        public bool Insert()
        {
            string sqlCommand = "INSERT INTO `teams` (`team_name`, `remarks`,`team_logo`) VALUES ('" + this.team_name + "', '" + this.remarks + "','" + this.team_logo + "');";

            try
            {
                using (OdbcConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString))
                {
                    connection.Open();
                    OdbcCommand command = new OdbcCommand(sqlCommand, connection);
                    using (command = new OdbcCommand(@"SELECT MAX(team_id) from teams;", connection))
                    {
                        this.team_id = (int)command.ExecuteScalar();

                        connection.Close();
                        return true;
                    }                        
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete()
        {
            string sqlCommand1 = "DELETE FROM teams where team_id = "+this.team_id;
            string sqlCommand2 = "DELETE FROM matches where home_team = " + this.team_id + " OR away_team = " + this.team_id;

            try
            {
                using (OdbcConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString))
                {
                    connection.Open();

                    OdbcCommand command = new OdbcCommand(sqlCommand1, connection);
                    command.ExecuteNonQuery();
                    command = new OdbcCommand(sqlCommand2, connection);
                    command.ExecuteNonQuery();

                    connection.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public static ArrayList getAll()
        {
            ArrayList allTeams = new ArrayList();

            string sqlCommand = @"select * from teams;";
            try
            {
                using (OdbcConnection connection = new OdbcConnection(ConfigurationManager.ConnectionStrings["MySQLConnStr"].ConnectionString))
                {
                    connection.Open();
                    using (OdbcCommand command = new OdbcCommand(sqlCommand, connection))
                    using (OdbcDataReader dr = command.ExecuteReader())
                    {
                        while (dr.Read())
                        {

                            TeamModel t = new TeamModel();
                            t.remarks = dr["remarks"].ToString();
                            t.team_logo = dr["team_logo"].ToString();
                            t.team_name = dr["team_name"].ToString();
                            t.team_id = Int32.Parse(dr["team_id"].ToString());

                            allTeams.Add(t);
                        }

                        
                        dr.Close();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return allTeams;
            }
            return allTeams;
        }
    }
}